# Installation

 Clone the repository to your local machine:

   ```bash
   git clone https://gitlab.com/shaik-rakhaib/ipl-project-django.git

   cd ipl-project-django
   ```
* After cloning the repository, it is recommended to install a virtual environment to run the desired python file.
  
* To install a virtual environment, run this command in your terminal (make sure to run it in same directory as the python files are in)
  ```bash
  python3 -m venv 'env_name'
  ```
* To activate the virtual environment, run this command,
  ```bash
  source 'env_name'/bin/activate
  ```
* If everything has gone well without any issues, your shell prompt(python in this case) should start with the name of your virtual environment and should look something like this.
  ```bash
  (my_env) shaik-rakhaib@shaik-aspire:~
  ```
* In my case , 'my_env' is virtual environment that I activated.

## Django Installation:

- You can install Django framework using the following command.

    ```bash
    pip install django
    ```
- After successful installation of Django, you can start the server by,

    ```bash
    python3 manage.py runserver
    ```
    - You might come across some migration issue here, to resolve it,
    - You need to download [these](https://www.kaggle.com/manasgarg/ipl/version/5) files and later
    -   Use this command,
        ```bash
        python3 manage.py makemigrations
        ```
        now,

        ```bash
        python3 manage.py migrate
        ```
    - Finally use this command below to import the csv data to Django models
        ```bash
        python3 manage.py import_csv <path/to/matches.csv> <path/to/deliveries.csv>
        ```
- After all the errors are resolved, you can navigate to the localhost where the server is running
- You will find a list of links which lead to all the json data respective to it.
- If you need to go to specific question through the url, you can try these 
    - localhost:8000/ipl/1: 'matches_in_year'
    - localhost:8000/ipl/2: 'won_per_year'
    - localhost:8000/ipl/3: 'extra_runs_conceded'
    - localhost:8000/ipl/4: 'top_eco_bowlers'
    ### You can add /chart at the end of the urls to go their specific charts respectively.