from django.urls import path
from django.views.generic import RedirectView


from . import views

urlpatterns = [

    path('', RedirectView.as_view(url='ipl/')),
    path('ipl/', views.home, name='home'),

    path('ipl/<int:q>/', views.question_number, name='question_number'),

    path('ipl/matches_in_year/', views.matches_in_year, name='matches_in_year'),
    path('ipl/won_per_year/', views.won_per_year, name='won_per_year'),
    path('ipl/extra_runs_conceded/', views.extra_runs_conceded,name='extra_runs_conceded'),
    path('ipl/top_eco_bowlers/', views.top_eco_bowlers, name='top_eco_bowlers'),

    path('ipl/matches_in_year/chart', views.matches_in_year_chart, name='matches_in_year_chart'),
    path('ipl/won_per_year/chart', views.won_per_year_chart, name='won_per_year_chart'),
    path('ipl/extra_runs_conceded/chart', views.extra_runs_conceded_chart, name='extra_runs_conceded_chart'),
    path('ipl/top_eco_bowlers/chart', views.top_eco_bowlers_chart, name='top_eco_bowlers_chart'),

]
