import csv
from django.core.management.base import BaseCommand
from django.db import transaction
from IPLapp.models import Matches, Deliveries

class Command(BaseCommand):
    help = 'Import data from CSV files into Django models'
    
    def add_arguments(self, parser):
        parser.add_argument('matches_path', type=str, help='Path to cricket_match CSV file')
        parser.add_argument('deliveries_path', type=str, help='Path to deliveries CSV file')

    def handle(self, *args, **kwargs):
        matches_path = kwargs['matches_path']
        deliveries_path = kwargs['deliveries_path']
        try:
            with transaction.atomic():
                # importing data into matches model
                with open(matches_path, 'r') as csvfile:
                    csv_reader = csv.DictReader(csvfile)
                    # Matches.objects.all().delete()  
                    for row in csv_reader:
                        Matches.objects.create(**row)
                
                with open(deliveries_path, 'r') as csvfile:
                    # Importing data into deliveries model
                    csv_reader = csv.DictReader(csvfile)
                    Deliveries.objects.all().delete()  # Clear existing data
                    for row in csv_reader:
                        match_id = int(row.pop('match_id', None))
                        match_instance = Matches.objects.get(id=match_id)
                        row['match_id']=match_instance
                        
                        Deliveries.objects.create(**row)
                        
                        
                self.stdout.write(self.style.SUCCESS('Data import successful!'))

        except Exception as e:
            self.stdout.write(self.style.ERROR(f'error : {e}'))