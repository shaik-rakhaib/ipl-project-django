from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from .models import Matches, Deliveries
from django.urls import reverse
from django.db.models import Sum, F, Count
import json
import requests
# Create your views here.

match_count = Matches.objects.count()


def home(request):
    return render(request,'index.html')


def question_number(request, q):
    question_urls = {
        1: 'matches_in_year',
        2: 'won_per_year',
        3: 'extra_runs_conceded',
        4: 'top_eco_bowlers'
    }
    if (question_urls[q]):
        return HttpResponseRedirect(reverse(question_urls[q]))
    else:
        return HttpResponse("Invalid Question number")


def matches_in_year(request):
    matches_year = {}
    for i in range(1, match_count+1):
        match = Matches.objects.get(id=i)
        if (match.season not in matches_year):
            matches_year[match.season] = 1
        else:
            matches_year[match.season] += 1
    json_data = json.dumps(matches_year)
    # print(matches_year)
    return JsonResponse(json_data, safe=False)


def won_per_year(request):
    matches_won_year = {}
    for i in range(1, match_count+1):
        match = Matches.objects.get(id=i)
        if (match.winner):
            if (match.winner == 'Rising Pune Supergiant'):
                match.winner = 'Rising Pune Supergiants'
            if (match.winner not in matches_won_year):
                matches_won_year[match.winner] = {match.season: 1}
            else:
                if (match.season not in matches_won_year[match.winner]):
                    matches_won_year[match.winner][match.season] = 1
                else:
                    matches_won_year[match.winner][match.season] += 1
    json_data = json.dumps(matches_won_year)
    # print(matches_won_year)
    return JsonResponse(json_data, safe=False)


def extra_runs_conceded(request):
    extra_runs_2016 = (
        Deliveries.objects
        .filter(match_id__date__year=2016)
        .values('bowling_team')
        .annotate(total_extra_runs=Sum(F('extra_runs'))))
    # print(extra_runs_2016)
    extra_runs_2016 = list(extra_runs_2016)
    return JsonResponse(extra_runs_2016, safe=False)


def top_eco_bowlers(request):
    matches_2015 = (
        Deliveries.objects.filter(match_id__date__year=2015)
        .values('bowler')
        .annotate(eco_rate=(Sum('total_runs')*6.0/Count('bowler')))
        .order_by('eco_rate'))[:10]
    #  print(matches_2015)
    matches_2015 = list((matches_2015))
    return JsonResponse(matches_2015, safe=False)


def matches_in_year_chart(request):
    matches_in_year_url = reverse('matches_in_year')
    response = requests.get(request.build_absolute_uri(matches_in_year_url))
    if response.status_code == 200:
        json_data = response.json()
        # print(json_data)
        return render(request, 'matches_in_year_chart.html', {'data': json_data})
    else:
        return HttpResponse("error retrieving data")


def won_per_year_chart(request):
    won_per_year_url = reverse('won_per_year')
    response = requests.get(request.build_absolute_uri(won_per_year_url))
    if response.status_code == 200:
        json_data = response.json()
        # print(json_data)
        return render(request, 'won_per_year_chart.html', {'data': json_data})
    else:
        return HttpResponse("error retrieving data")


def extra_runs_conceded_chart(request):
    extra_runs_conceded_url = reverse('extra_runs_conceded')
    response = requests.get(
        request.build_absolute_uri(extra_runs_conceded_url))
    if response.status_code == 200:
        json_data = response.json()
        # print(json.dumps(json_data))
        context = {'data': json.dumps(json_data)}
        return render(request, 'extra_runs_concede.html', context)


def top_eco_bowlers_chart(request):
    top_eco_bowlers_url = reverse('top_eco_bowlers')
    response = requests.get(request.build_absolute_uri(top_eco_bowlers_url))
    if response.status_code == 200:
        json_data = response.json()
        # print(json.dumps(json_data))
        context = {'data': json.dumps(json_data)}
        return render(request, 'top_eco_bowlers_chart.html', context)
